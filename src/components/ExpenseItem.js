import React from 'react'
import ExpenseDate from './ExpenseDate'
import Card from './Card'

const ExpenseItem = (props) => {
    return (
        <Card className="item"> 
            <h2>{props.title}</h2>
            <span>{props.amount}</span><br />
            <ExpenseDate date={props.date} />
        </Card>

    )
}

export default ExpenseItem
